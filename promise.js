//შევქმნათ მასივი
const posts = [
    {title:'post one', body:'this is post one'},
    {title:'post two', body:'this is post two'}
]
//შევქმნათ ფუნქცია რომელშიც გამოვიყენებტ settimeout-ს რადგან ჩვენი მასივიდან განკუთვნილი
//დროის შემდეგ გამოიტამოს გადაცემული ობიექტი
function getposts() {
    setTimeout(() =>{
      let output = ''
      //დავუწეროთ foreach ციკლი რათა ციკლი დაატრიალოს მასივის ყველა ელემენტზე
      //შევქმნატ ლისტი და შიგნით გამოვიტანოთ ზემოთ მოცემული მასივიდან სახელი
      //ეს ყოველივე შევითანოთ ზემოთ აღნიშნულ output-ში რატა ყოველ შემდეგ მოძრაობაზე გამოიტანოს ობიექტი
      //output შევიტანოტ html ის body-ში
      posts.forEach((post) =>{
        output += `<li>${post.title}</li>`
      })
      document.body.innerHTML = output
    },1000)
}
ს
// შექმნილ პოსტს ვაგდებთ ლისთში .push გამოყენებით და ვაძლევთ დაგვიანების დროს 2 წამს
//ამის შემდეგ ვაბრნებთ ახალ promise და გადავცემთ ორ პარამეტრს (resolve-გადაწყვეტა , reject - დაუარება) 
// შემდეგ ვქმნით ხელოვნურ ერორს და ვამოწმებთ false-ზე თუ ერორი არ მოიძებნება ვაძლევთ გადაწყვეთის უფლებას 
//თუ ერორი ამოაგდო ესეიგი შევუქმენით ხელოვნური ერორი ამ კონკრეტულ ამოცანაში ვაძლევთ დაუარების უფლებას
function createPost(post){
   return new Promise((resolve , reject)=>{
    setTimeout(()=>{
        posts.push(post)
        const error = false
    if (!error) {
        resolve()
    }else{
        reject("რაღაც არასწორია")
    }
    },2000)
    
   })

}

// შევქმნათ პოსტი და დავამატოთ მეორე პარამეტრად პირველი ფუნქცია რათა დაელოდოს და ორივე ფუნქცია ერთდროულად გამოიტანოს
//.catch ოპერატორით ვიჭერთ ერორის საშიშროებას
//---------------------------------------------------------------------------------------------------------------------------
createPost(
    {title:'post one', body:'this is post three'}
).then(getposts).catch(err => console.log(err))

// //Async / Await
// // await-ის დახმარებით ჩვენ ვაძლევთ მითითებას  ახალ შექმნილ პოსტს რომ დაელოდოს ქვემოთ მოცემულ ფუნქციას ამ შემთხვევაში 
// //getpost() ფუნქციას
// async function magaliti(){
//     await createPost({title:'post three', body:'this is post three'})
//     getposts()
// }
// magaliti()