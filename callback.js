//შევქმნათ მასივი
const posts = [
    {title:'post one', body:'this is post one'},
    {title:'post two', body:'this is post two'}
]
//შევქმნათ ფუნქცია რომელშიც გამოვიყენებტ settimeout-ს რადგან ჩვენი მასივიდან განკუთვნილი
//დროის შემდეგ გამოიტამოს გადაცემული ობიექტი
function getposts() {
    setTimeout(() =>{
      let output = ''
      //დავუწეროთ foreach ციკლი რათა ციკლი დაატრიალოს მასივის ყველა ელემენტზე
      //შევქმნატ ლისტი და შიგნით გამოვიტანოთ ზემოთ მოცემული მასივიდან სახელი
      //ეს ყოველივე შევითანოთ ზემოთ აღნიშნულ output-ში რატა ყოველ შემდეგ მოძრაობაზე გამოიტანოს ობიექტი
      //output შევიტანოტ html ის body-ში
      posts.forEach((post) =>{
        output += `<li>${post.title}</li>`
      })
      document.body.innerHTML = output
    },1000)
}

// შექმნილ პოსტს ვაგდებთ ლისთში .push გამოყენებით და ვაძლევთ დაგვიანების დროს 2 წამს
//ამ ყოველივეს გამო თუ გადავამოწმებთ კოდი არაფერს გამოიტანს აი აქ შემოდის callback-ები
function createPost(post,callback){
setTimeout(()=>{
    posts.push(post)
    callback()
},2000)
}

// შევქმნათ პოსტი და დავამატოთ მეორე პარამეტრად პირველი ფუნქცია რათა დაელოდოს და ორივე ფუნქცია ერთდროულად გამოიტანოს
createPost(
    {title:'post one', body:'this is post three'},
    getposts
)